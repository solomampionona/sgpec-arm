/**
 * Created by laiso on 19/11/2015.
 */

var loader = $("#loader").data('dialog');
var confirm = $('#dialog-confirm').data('dialog');

function showConfirm() {
    confirm.open();
}
function closeConfirm(){
    confirm.close();
}

function showDialog(data) {
    var close = "<button class='button default place-left' onclick=\"$('#dialog').data('dialog').close()\">Fermer</button>";
    $('#dialog-container').html(data).append(close);
    var dialog = $('#dialog').data('dialog');
    dialog.open();
}

$(function () {
    $('.scroll-y').mCustomScrollbar({
        theme: "dark-3", axis: "y",
    });

    $('.scroll-x').mCustomScrollbar({
        theme: "dark-3", axis: "x",
    });

    $('.closeFlash').click(function () {
        var flash = "#" + $(this).attr('flashId');
        $(flash).slideUp();
    });

    $('.ajax-button').click(function () {
        loader.open();

        var target = $(this).attr('href');

        $.ajax({
            url: target,
            method: 'get',
            success: function (data) {
                loader.close();
                showDialog(data);
            },
            error: function (xhr, ajaxOptions, thrownError) {
                loader.close();
                showDialog("<h1 class='error '>Erreur : " + thrownError + "</h1><div style='max-height: 300px; overflow-y: scroll' >" + xhr.responseText +  "</div>");
            }
        });
        return false;
    });

    $('.ajax-link').click(function () {
        var id = $(this).attr('data-id');
        alert(id);
        return false;
    })
});