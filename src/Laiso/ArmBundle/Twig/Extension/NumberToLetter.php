<?php

namespace Laiso\ArmBundle\Twig\Extension;

class NumberToLetter extends \Twig_Extension
{
    /**
     * {@inheritdoc}
     */
    public function getName()
    {
        return 'number_to_letter_extension';
    }

    public function getFilters()
    {
        return array(
            new \Twig_SimpleFilter('numberToLetter', array($this, 'toLetter')),
        );
    }

    public function toLetter($nombre)
    {
        return $nombre . " toLetter";
    }
}
