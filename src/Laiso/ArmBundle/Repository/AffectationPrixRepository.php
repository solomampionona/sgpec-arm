<?php

namespace Laiso\ArmBundle\Repository;

/**
 * AffectationPrixRepository
 *
 * This class was generated by the Doctrine ORM. Add your own custom
 * repository methods below.
 */
class AffectationPrixRepository extends \Doctrine\ORM\EntityRepository
{
    public function findByConsistance($consistance)
    {
        $query = $this->createQueryBuilder("q")
            ->select("q")
            ->where("q.consistance = :consistance")
            ->orderBy("q.id")
            ->setParameter("consistance", $consistance)
            ->getQuery();
        return $query->getResult();
    }
}
