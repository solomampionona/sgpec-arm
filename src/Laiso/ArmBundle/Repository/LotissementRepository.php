<?php

namespace Laiso\ArmBundle\Repository;

/**
 * LotissementRepository
 *
 * This class was generated by the Doctrine ORM. Add your own custom
 * repository methods below.
 */
class LotissementRepository extends \Doctrine\ORM\EntityRepository
{
    public function findAllByBloc($bloc){
        $query = $this->createQueryBuilder("q")
            ->select("q")
            ->where("q.bloc = :bloc")
            //->andWhere("q.valide = false")
            ->orderBy("q.id")
            ->setParameter("bloc", $bloc)
            ->getQuery();
        return $query->getResult();
    }
}
