<?php

namespace Laiso\ArmBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use Laiso\ArmBundle\Entity\LibelleAvenant;
use Laiso\ArmBundle\Form\LibelleAvenantType;

/**
 * LibelleAvenant controller.
 *
 */
class LibelleAvenantController extends Controller
{

    /**
     * Lists all LibelleAvenant entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('LaisoArmBundle:LibelleAvenant')->findAll();

        return $this->render('LaisoArmBundle:LibelleAvenant:index.html.twig', array(
            'entities' => $entities,
        ));
    }
    /**
     * Creates a new LibelleAvenant entity.
     *
     */
    public function createAction(Request $request)
    {
        $entity = new LibelleAvenant();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('libelleavenant'));
        }

        return $this->render('LaisoArmBundle:LibelleAvenant:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Creates a form to create a LibelleAvenant entity.
     *
     * @param LibelleAvenant $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(LibelleAvenant $entity)
    {
        $form = $this->createForm(new LibelleAvenantType(), $entity, array(
            'action' => $this->generateUrl('libelleavenant_create'),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Créer', 'attr' => array(
            'class' => 'button success place-right'
        )));

        return $form;
    }

    /**
     * Displays a form to create a new LibelleAvenant entity.
     *
     */
    public function newAction(Request $request)
    {
        $entity = new LibelleAvenant();
        $form   = $this->createCreateForm($entity);

        if($request->isXmlHttpRequest())
            return $this->render('LaisoArmBundle:LibelleAvenant/includes:new_libelleAvenant.html.twig', array(
                'entity' => $entity,
                'form'   => $form->createView(),
            ));

        return $this->render('LaisoArmBundle:LibelleAvenant:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Finds and displays a LibelleAvenant entity.
     *
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('LaisoArmBundle:LibelleAvenant')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find LibelleAvenant entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return $this->render('LaisoArmBundle:LibelleAvenant:show.html.twig', array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing LibelleAvenant entity.
     *
     */
    public function editAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('LaisoArmBundle:LibelleAvenant')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find LibelleAvenant entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        if($request->isXmlHttpRequest())
            return $this->render('LaisoArmBundle:LibelleAvenant/includes:edit_libelleAvenant.html.twig', array(
                'entity'      => $entity,
                'edit_form'   => $editForm->createView(),
                'delete_form' => $deleteForm->createView(),
            ));

        return $this->render('LaisoArmBundle:LibelleAvenant:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
    * Creates a form to edit a LibelleAvenant entity.
    *
    * @param LibelleAvenant $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(LibelleAvenant $entity)
    {
        $form = $this->createForm(new LibelleAvenantType(), $entity, array(
            'action' => $this->generateUrl('libelleavenant_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Mettre à jour', 'attr' => array(
            'class' => 'button success place-right'
        )));

        return $form;
    }
    /**
     * Edits an existing LibelleAvenant entity.
     *
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('LaisoArmBundle:LibelleAvenant')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find LibelleAvenant entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();

            return $this->redirect($this->generateUrl('libelleavenant'));
        }

        return $this->render('LaisoArmBundle:LibelleAvenant:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }
    /**
     * Deletes a LibelleAvenant entity.
     *
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('LaisoArmBundle:LibelleAvenant')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find LibelleAvenant entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('libelleavenant'));
    }

    /**
     * Creates a form to delete a LibelleAvenant entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('libelleavenant_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array('label' => 'Supprimer', 'attr' => array(
                'class' => 'button danger place-right'
            )))
            ->getForm()
        ;
    }
}
