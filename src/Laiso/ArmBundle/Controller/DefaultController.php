<?php

namespace Laiso\ArmBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    public function indexAction()
    {
        return $this->render('LaisoArmBundle:Default:index.html.twig');
    }
}
