<?php

namespace Laiso\ArmBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use Laiso\ArmBundle\Entity\Localisation;
use Laiso\ArmBundle\Form\LocalisationType;

/**
 * Localisation controller.
 *
 */
class LocalisationController extends Controller
{
    /**
     * Creates a new Localisation entity.
     *
     */
    public function createAction(Request $request, $consistanceId)
    {
        $entity = new Localisation();
        $form = $this->createCreateForm($entity, $consistanceId);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('dao_show', array(
                'id' => $entity->getConsistance()->getLotissement()->getId(),
            )));
        }

        return $this->render('LaisoArmBundle:Localisation:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
            'consistanceId' => $consistanceId
        ));
    }

    /**
     * Creates a form to create a Localisation entity.
     *
     * @param Localisation $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(Localisation $entity, $consistanceId)
    {
        $form = $this->createForm(new LocalisationType(), $entity, array(
            'action' => $this->generateUrl('localisation_create', array(
                'consistanceId' => $consistanceId,
            )),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Créer', 'attr' => array(
            'class' => 'button success place-right'
        )));

        return $form;
    }

    /**
     * Displays a form to create a new Localisation entity.
     *
     */
    public function newAction(Request $request, $consistanceId)
    {
        $entity = new Localisation();
        $consistance = $this->getDoctrine()->getRepository("LaisoArmBundle:Consistance")->find($consistanceId);
        if($consistance != null)
            $entity->setConsistance($consistance);
        $form   = $this->createCreateForm($entity, $consistanceId);

        if($request->isXmlHttpRequest())
            return $this->render('@LaisoArm/Localisation/includes/new_localisation.html.twig', array(
                'entity' => $entity,
                'form'   => $form->createView(),
                'consistanceId' => $consistanceId,
            ));

        return $this->render('LaisoArmBundle:Localisation:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
            'consistanceId' => $consistanceId,
        ));
    }

    /**
     * Displays a form to edit an existing Localisation entity.
     *
     */
    public function editAction(Request $request, $consistanceId, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('LaisoArmBundle:Localisation')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Localisation entity.');
        }

        $editForm = $this->createEditForm($entity, $consistanceId);
        $deleteForm = $this->createDeleteForm($id, $consistanceId);

        if($request->isXmlHttpRequest())
            return $this->render('LaisoArmBundle:Localisation/includes:edit_localisation.html.twig', array(
                'entity'      => $entity,
                'edit_form'   => $editForm->createView(),
                'delete_form' => $deleteForm->createView(),
            ));

        return $this->render('LaisoArmBundle:Localisation:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
    * Creates a form to edit a Localisation entity.
    *
    * @param Localisation $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(Localisation $entity, $consistanceId)
    {
        $form = $this->createForm(new LocalisationType(), $entity, array(
            'action' => $this->generateUrl('localisation_update', array(
                'id' => $entity->getId(),
                'consistanceId' => $consistanceId,
            )),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Mettre à jour', 'attr' => array(
            'class' => 'button place-right success'
        )));

        return $form;
    }
    /**
     * Edits an existing Localisation entity.
     *
     */
    public function updateAction(Request $request, $id, $consistanceId)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('LaisoArmBundle:Localisation')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Localisation entity.');
        }

        $deleteForm = $this->createDeleteForm($id, $consistanceId);
        $editForm = $this->createEditForm($entity, $consistanceId);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();
            return$this->redirect($this->generateUrl('dao_show', array('id' => $entity->getConsistance()->getLotissement()->getId())));
        }

        return $this->render('LaisoArmBundle:Localisation:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }
    /**
     * Deletes a Localisation entity.
     *
     */
    public function deleteAction(Request $request, $id, $consistanceId)
    {
        $form = $this->createDeleteForm($id, $consistanceId);
        $form->handleRequest($request);
        $em = $this->getDoctrine()->getManager();
        if ($form->isValid()) {
            $entity = $em->getRepository('LaisoArmBundle:Localisation')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find Localisation entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('dao_show', array(
            'id' => $em->getRepository('LaisoArmBundle:Consistance')->find($consistanceId)->getLotissement()->getId()
        )));
    }

    /**
     * Creates a form to delete a Localisation entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id, $consistanceId)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('localisation_delete', array(
                'id' => $id,
                'consistanceId' => $consistanceId,
            )))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array('label' => 'Supprimer', 'attr' => array(
                'class' => 'button danger place-right'
            )))
            ->getForm()
        ;
    }

    /**
     * Handle Ajax request for deletion
     *
     * (c) Laiso
     *
     * @param Request $request
     * @param $id
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function deleteAjaxAction(Request $request, $id, $consistanceId)
    {
        $form = $this->createDeleteForm($id, $consistanceId);
        $form->handleRequest($request);

        $entity = $this->getDoctrine()->getRepository('LaisoArmBundle:Localisation')->find($id);
        if(!$entity)
            return $this->createNotFoundException("Localisation introuvable");

        if ($request->isXmlHttpRequest())
            return $this->render("LaisoArmBundle:Shared:delete_ajax.html.twig", array(
                'delete_form' => $form->createView(),
                'title' => "Supprimer la localisation \"". $entity->getLocalisation() . "\""
            ));
        else return $this->redirectToRoute("dao_show", array(
            'id' => $this->getDoctrine()->getRepository('LaisoArmBundle:Consistance')->find($consistanceId)->getLotissement()->getId()
        ));
    }
}
