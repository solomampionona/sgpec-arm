<?php

namespace Laiso\ArmBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use Laiso\ArmBundle\Entity\SerieDePrix;
use Laiso\ArmBundle\Form\SerieDePrixType;

/**
 * SerieDePrix controller.
 *
 */
class SerieDePrixController extends Controller
{

    /**
     * Lists all SerieDePrix entities.
     *
     */
    public function indexAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('LaisoArmBundle:SerieDePrix')->findAll();
        $paginator = $this->get('knp_paginator');
        return $this->render('LaisoArmBundle:SerieDePrix:index.html.twig', array(
            'entities' => $paginator->paginate($entities, $request->query->getInt('page', 1), 10),
        ));
    }
    /**
     * Creates a new SerieDePrix entity.
     *
     */
    public function createAction(Request $request)
    {
        $entity = new SerieDePrix();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('prix_serie'));
        }

        return $this->render('LaisoArmBundle:SerieDePrix:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Creates a form to create a SerieDePrix entity.
     *
     * @param SerieDePrix $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(SerieDePrix $entity)
    {
        $form = $this->createForm(new SerieDePrixType(), $entity, array(
            'action' => $this->generateUrl('prix_serie_create'),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Créer' , 'attr' => array(
            'class' => 'button success place-right'
        )));

        return $form;
    }

    /**
     * Displays a form to create a new SerieDePrix entity.
     *
     */
    public function newAction(Request $request)
    {
        $entity = new SerieDePrix();
        $form   = $this->createCreateForm($entity);

        if($request->isXmlHttpRequest())
            return $this->render('LaisoArmBundle:SerieDePrix/includes:new_serieDePrix.html.twig', array(
                'entity' => $entity,
                'form'   => $form->createView(),
            ));

        return $this->render('LaisoArmBundle:SerieDePrix:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Finds and displays a SerieDePrix entity.
     *
     */
    public function showAction(Request $request, $numero)
    {
        $em = $this->getDoctrine()->getManager();

        $s = $request->query->get('s');

        if(null == $s)
            $entity = $em->getRepository('LaisoArmBundle:SerieDePrix')->findOneBy(array(
                'numero' => $numero
            ));
        else
            $entity = $em->getRepository('LaisoArmBundle:SerieDePrix')->findOneBy(array(
                'numero' => $numero,
                'sousCategorie' => $s
            ));

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find SerieDePrix entity.');
        }

        $deleteForm = $this->createDeleteForm($numero, $s);

        return $this->render('LaisoArmBundle:SerieDePrix:show.html.twig', array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing SerieDePrix entity.
     *
     */
    public function editAction(Request $request, $numero)
    {
        $em = $this->getDoctrine()->getManager();
        $s = $request->query->get('s');
        if(null == $s)
            $entity = $em->getRepository('LaisoArmBundle:SerieDePrix')->findOneBy(array(
                'numero' => $numero
            ));
        else
            $entity = $em->getRepository('LaisoArmBundle:SerieDePrix')->findOneBy(array(
                'numero' => $numero,
                'sousCategorie' => $s,
            ));

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find SerieDePrix entity.');
        }

        $editForm = $this->createEditForm($entity, $s);
        $deleteForm = $this->createDeleteForm($numero, $s);

        if($request->isXmlHttpRequest())
            return $this->render('LaisoArmBundle:SerieDePrix/includes:edit_serieDePrix.html.twig', array(
                'entity'      => $entity,
                'edit_form'   => $editForm->createView(),
                'delete_form' => $deleteForm->createView(),
            ));

        return $this->render('LaisoArmBundle:SerieDePrix:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
    * Creates a form to edit a SerieDePrix entity.
    *
    * @param SerieDePrix $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(SerieDePrix $entity, $s)
    {
        $form = $this->createForm(new SerieDePrixType(), $entity, array(
            'action' => $this->generateUrl('prix_serie_update', array(
                'numero' => $entity->getNumero(),
                's' => $s,
            )),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Mettre à jour', 'attr' => array(
            'class' => 'button success place-right'
        )));

        return $form;
    }
    /**
     * Edits an existing SerieDePrix entity.
     *
     */
    public function updateAction(Request $request, $numero)
    {
        $em = $this->getDoctrine()->getManager();

        $s = $request->query->get('s');
        if($s == null)
            $entity = $em->getRepository('LaisoArmBundle:SerieDePrix')->findOneBy(array(
                'numero' => $numero
            ));
        else
            $entity = $em->getRepository('LaisoArmBundle:SerieDePrix')->findOneBy(array(
                'numero' => $numero,
                'sousCategorie' => $s
            ));
        if (!$entity) {
            throw $this->createNotFoundException('Unable to find SerieDePrix entity.');
        }

        $deleteForm = $this->createDeleteForm($numero, $s);
        $editForm = $this->createEditForm($entity, $s);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();

            return $this->redirect($this->generateUrl('prix_serie'));
        }

        return $this->render('LaisoArmBundle:SerieDePrix:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }
    /**
     * Deletes a SerieDePrix entity.
     *
     */
    public function deleteAction(Request $request, $numero)
    {
        $s = $request->query->get('s');
        $form = $this->createDeleteForm($numero, $s);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();

            if($s == null)
                $entity = $em->getRepository('LaisoArmBundle:SerieDePrix')->findOneBy(array(
                    'numero' => $numero
                ));
            else
                $entity = $em->getRepository('LaisoArmBundle:SerieDePrix')->findOneBy(array(
                    'numero' => $numero,
                    'sousCategorie' => $s,
                ));

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find SerieDePrix entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('prix_serie'));
    }

    /**
     * Creates a form to delete a SerieDePrix entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($numero, $s)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('prix_serie_delete', array(
                'numero' => $numero,
                's' => $s
            )))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array('label' => 'Supprimer', 'attr' => array(
                'class' => 'button danger place-right'
            )))
            ->getForm()
        ;
    }

    /**
     * Handle Ajax request for deletion
     *
     * (c) Laiso
     *
     * @param Request $request
     * @param $numero
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function deleteAjaxAction(Request $request, $numero)
    {
        $s = $request->query->get('s');

        $form = $this->createDeleteForm($numero, $s);
        $form->handleRequest($request);

        if($s == null)
            $entity = $this->getDoctrine()->getRepository('LaisoArmBundle:SerieDePrix')->findOneBy(array(
                'numero' => $numero
            ));
        else
            $entity = $this->getDoctrine()->getRepository('LaisoArmBundle:SerieDePrix')->findOneBy(array(
                'numero' => $numero,
                'sousCategorie' => $s
            ));

        if(!$entity)
            return $this->createNotFoundException("Prix introuvable");

        if ($request->isXmlHttpRequest()) {
            if ($s == null)
                return $this->render("LaisoArmBundle:Shared:delete_ajax.html.twig", array(
                    'delete_form' => $form->createView(),
                    'title' => "Supprimer le prix numéro \"" . $entity->getNumero() . "\""
                ));
            else
                return $this->render("LaisoArmBundle:Shared:delete_ajax.html.twig", array(
                    'delete_form' => $form->createView(),
                    'title' => "Supprimer le prix numéro \"" . $entity->getNumero() . $entity->getSousCategorie() . "\""
                ));
        }
        else {
            if ($s == null)
                return $this->redirectToRoute("prix_serie_show", array('numero' => $numero));
            else
                return $this->redirectToRoute("prix_serie_show", array('numero' => $numero, 's' => $s));
        }
    }
}
