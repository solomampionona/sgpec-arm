<?php

namespace Laiso\ArmBundle\Controller;

use Laiso\ArmBundle\Entity\AffectationPrix;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class AffectationPrixController extends Controller
{
    public function addPrixUnitaireAction(Request $request, $numero, $marcheId, $consistanceId)
    {
        $em = $this->getDoctrine()->getManager();

        $affectationPrix = $this->getAffectationPrix($consistanceId, $numero);

        if(!$affectationPrix)
            throw new NotFoundHttpException('Affectation Prix non trouvé');

        if($request->isXmlHttpRequest())
            return $this->render("@LaisoArm/AffectationPrix/content.html.twig", array(
                'form' => $this->getForm($marcheId, $consistanceId, $numero)->createView(),
                'entity' => $affectationPrix
            ));

        return $this->render("@LaisoArm/AffectationPrix/addPrixUnitaire.html.twig", array(
            'form' => $this->getForm($marcheId, $consistanceId, $numero)->createView(),
            'entity' => $affectationPrix
        ));
    }

    private function getForm($marcheid, $consistanceId, $numero)
    {
        $builder = $this->get('form.factory')->createBuilder('form');
        $builder->setMethod('POST');
        $builder->setAction($this->generateUrl('prix_unitaire_update', array(
            'consistanceId' => $consistanceId,
            'marcheId' => $marcheid,
            'numero' => $numero,
        )));
        $builder->add('prixUnitaire', null, array(
            'attr' => array(
                'placeholder' => 'Prix unitaire'
            )
        ));
        $builder->add('submit', 'submit', array(
            'label' => 'Valider',
            'attr' => array(
                'class' => 'button place-right success'
            )
        ));
        return $builder->getForm();
    }

    public function createAction(Request $request, $numero, $marcheId, $consistanceId)
    {
        $affectationPrix = $this->getAffectationPrix($consistanceId, $numero);

        if(!$affectationPrix)
            throw new NotFoundHttpException('Affectation Prix non trouvé');

        $form = $this->getForm($marcheId, $consistanceId, $numero);
        $form->handleRequest($request);
        if($form->isValid()){
            $affectationPrix->setPrixUnitaire($form->getData()['prixUnitaire']);
            $em = $this->getDoctrine()->getManager();

            $em->persist($affectationPrix);
            $em->flush();

            return $this->redirect($this->generateUrl('avenant_show', array(
                'numero' => 0,
                'marcheId' => $marcheId,
            )));
        }
        return $this->render("@LaisoArm/AffectationPrix/addPrixUnitaire.html.twig", array(
            'form' => $this->getForm($marcheId, $consistanceId, $numero)->createView(),
            'entity' => $affectationPrix
        ));
    }

    /**
     * @param $consistanceId
     * @param $numero
     * @return AffectationPrix|null|object
     */
    private function getAffectationPrix($consistanceId, $numero)
    {
        $em = $this->getDoctrine()->getManager();
        return $affectationPrix = $em->getRepository('LaisoArmBundle:AffectationPrix')->findOneBy(array(
            'consistance' => $consistanceId,
            'serie' => $em->getRepository('LaisoArmBundle:SerieDePrix')->findOneBy(array(
                'numero' => $numero,
            )),
        ));
    }
}
