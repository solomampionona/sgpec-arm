<?php

namespace Laiso\ArmBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use Laiso\ArmBundle\Entity\UniteDeMesure;
use Laiso\ArmBundle\Form\UniteDeMesureType;

/**
 * UniteDeMesure controller.
 *
 */
class UniteDeMesureController extends Controller
{

    /**
     * Lists all UniteDeMesure entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('LaisoArmBundle:UniteDeMesure')->findAll();

        return $this->render('LaisoArmBundle:UniteDeMesure:index.html.twig', array(
            'entities' => $entities,
        ));
    }
    /**
     * Creates a new UniteDeMesure entity.
     *
     */
    public function createAction(Request $request)
    {
        $entity = new UniteDeMesure();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('unitedemesure'));
        }

        return $this->render('LaisoArmBundle:UniteDeMesure:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Creates a form to create a UniteDeMesure entity.
     *
     * @param UniteDeMesure $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(UniteDeMesure $entity)
    {
        $form = $this->createForm(new UniteDeMesureType(), $entity, array(
            'action' => $this->generateUrl('unitedemesure_create'),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Créer', 'attr' => array(
            'class' => 'button success place-right'
        )));

        return $form;
    }

    /**
     * Displays a form to create a new UniteDeMesure entity.
     *
     */
    public function newAction(Request $request)
    {
        $entity = new UniteDeMesure();
        $form   = $this->createCreateForm($entity);

        if($request->isXmlHttpRequest())
            return $this->render('LaisoArmBundle:UniteDeMesure/includes:new_uniteDeMesure.html.twig', array(
                'entity' => $entity,
                'form'   => $form->createView(),
            ));

        return $this->render('LaisoArmBundle:UniteDeMesure:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Finds and displays a UniteDeMesure entity.
     *
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('LaisoArmBundle:UniteDeMesure')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find UniteDeMesure entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return $this->render('LaisoArmBundle:UniteDeMesure:show.html.twig', array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing UniteDeMesure entity.
     *
     */
    public function editAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('LaisoArmBundle:UniteDeMesure')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find UniteDeMesure entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        if($request->isXmlHttpRequest())
            return $this->render('LaisoArmBundle:UniteDeMesure/includes:edit_uniteDeMesure.html.twig', array(
                'entity'      => $entity,
                'edit_form'   => $editForm->createView(),
                'delete_form' => $deleteForm->createView(),
            ));

        return $this->render('LaisoArmBundle:UniteDeMesure:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
    * Creates a form to edit a UniteDeMesure entity.
    *
    * @param UniteDeMesure $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(UniteDeMesure $entity)
    {
        $form = $this->createForm(new UniteDeMesureType(), $entity, array(
            'action' => $this->generateUrl('unitedemesure_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Mettre à jour', 'attr' => array(
            'class' => 'button success place-right'
        )));

        return $form;
    }
    /**
     * Edits an existing UniteDeMesure entity.
     *
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('LaisoArmBundle:UniteDeMesure')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find UniteDeMesure entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();

            return $this->redirect($this->generateUrl('unitedemesure'));
        }

        return $this->render('LaisoArmBundle:UniteDeMesure:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }
    /**
     * Deletes a UniteDeMesure entity.
     *
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('LaisoArmBundle:UniteDeMesure')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find UniteDeMesure entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('unitedemesure'));
    }

    /**
     * Creates a form to delete a UniteDeMesure entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('unitedemesure_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array('label' => 'Supprimer', 'attr' => array(
                'class' => 'button danger place-right'
            )))
            ->getForm()
        ;
    }
}
