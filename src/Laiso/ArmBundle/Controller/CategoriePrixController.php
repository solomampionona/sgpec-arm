<?php

namespace Laiso\ArmBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use Laiso\ArmBundle\Entity\CategoriePrix;
use Laiso\ArmBundle\Form\CategoriePrixType;

/**
 * CategoriePrix controller.
 *
 */
class CategoriePrixController extends Controller
{

    /**
     * Lists all CategoriePrix entities.
     *
     */
    public function indexAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        /*
         * TODO: Search for %like%
         * if($request->query->get('t')){
            if($request->query->get('t') == 'numero')
                $entities = $em->getRepository("LaisoArmBundle:CategoriePrix")->findBy(array(
                    'numero' => $request->query->get('q')
                ));
            elseif($request->query->get('t') == 'designation')
                $entities = $em->getRepository("LaisoArmBundle:CategoriePrix")->findBy(array(
                    'designation' => "%".$request->query->get('q')."%"
                ));
        }
        else*/
        $entities = $em->getRepository('LaisoArmBundle:CategoriePrix')->findAll();
        $paginator = $this->get('knp_paginator');
        return $this->render('LaisoArmBundle:CategoriePrix:index.html.twig', array(
            'entities' => $paginator->paginate($entities, $request->query->getInt('page', 1), 10),
        ));
    }
    /**
     * Creates a new CategoriePrix entity.
     *
     */
    public function createAction(Request $request)
    {
        $entity = new CategoriePrix();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('prix_categorie'));
        }

        return $this->render('LaisoArmBundle:CategoriePrix:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Creates a form to create a CategoriePrix entity.
     *
     * @param CategoriePrix $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(CategoriePrix $entity)
    {
        $form = $this->createForm(new CategoriePrixType(), $entity, array(
            'action' => $this->generateUrl('prix_categorie_create'),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Créer', 'attr' => array(
            'class' => 'button success place-right'
        )));

        return $form;
    }

    /**
     * Displays a form to create a new CategoriePrix entity.
     *
     */
    public function newAction(Request $request)
    {
        $entity = new CategoriePrix();
        $form   = $this->createCreateForm($entity);

        if($request->isXmlHttpRequest())
            return $this->render('LaisoArmBundle:CategoriePrix/includes:new_categoriePrix.html.twig', array(
                'entity' => $entity,
                'form'   => $form->createView(),
            ));

        return $this->render('LaisoArmBundle:CategoriePrix:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Finds and displays a CategoriePrix entity.
     *
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('LaisoArmBundle:CategoriePrix')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find CategoriePrix entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return $this->render('LaisoArmBundle:CategoriePrix:show.html.twig', array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing CategoriePrix entity.
     *
     */
    public function editAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('LaisoArmBundle:CategoriePrix')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find CategoriePrix entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        if($request->isXmlHttpRequest())
            return $this->render('LaisoArmBundle:CategoriePrix/includes:edit_categoriePrix.html.twig', array(
                'entity'      => $entity,
                'edit_form'   => $editForm->createView(),
                'delete_form' => $deleteForm->createView(),
            ));

        return $this->render('LaisoArmBundle:CategoriePrix:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
    * Creates a form to edit a CategoriePrix entity.
    *
    * @param CategoriePrix $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(CategoriePrix $entity)
    {
        $form = $this->createForm(new CategoriePrixType(), $entity, array(
            'action' => $this->generateUrl('prix_categorie_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Mettre à jour', 'attr' => array(
            'class' => 'button success place-right'
        )));

        return $form;
    }
    /**
     * Edits an existing CategoriePrix entity.
     *
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('LaisoArmBundle:CategoriePrix')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find CategoriePrix entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();

            return $this->redirect($this->generateUrl('prix_categorie'));
        }

        return $this->render('LaisoArmBundle:CategoriePrix:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }
    /**
     * Deletes a CategoriePrix entity.
     *
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('LaisoArmBundle:CategoriePrix')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find CategoriePrix entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('prix_categorie'));
    }

    /**
     * Creates a form to delete a CategoriePrix entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('prix_categorie_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array('label' => 'Supprimer', 'attr' => array(
                'class' => 'button danger place-right'
            )))
            ->getForm()
        ;
    }

    /**
     * Handle Ajax request for deletion
     *
     * (c) Laiso
     *
     * @param Request $request
     * @param $id
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function deleteAjaxAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        $entity = $this->getDoctrine()->getRepository('LaisoArmBundle:CategoriePrix')->find($id);
        if(!$entity)
            return $this->createNotFoundException("Catégorie introuvable");

        if ($request->isXmlHttpRequest())
            return $this->render("LaisoArmBundle:Shared:delete_ajax.html.twig", array(
                'delete_form' => $form->createView(),
                'title' => "Supprimer la catégorie \"". $entity->getDesignation() . "\""
            ));
        else return $this->redirectToRoute("prix_categorie_show", array('id' => $id));
    }
}
