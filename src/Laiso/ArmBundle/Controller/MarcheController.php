<?php

namespace Laiso\ArmBundle\Controller;

use Laiso\ArmBundle\Utils\Consistance;
use Laiso\ArmBundle\Utils\DQE;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use Laiso\ArmBundle\Entity\Marche;
use Laiso\ArmBundle\Form\MarcheType;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;

/**
 * Marche controller.
 *
 */
class MarcheController extends Controller
{

    /**
     * Lists all Marche entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('LaisoArmBundle:Marche')->findBy(array(
            'lotissement' => $em->getRepository('LaisoArmBundle:Lotissement')->findBy(array(
                'valide' => true,
            ))
        ));

        return $this->render('LaisoArmBundle:Marche:index.html.twig', array(
            'entities' => $entities,
        ));
    }
    /**
     * Creates a new Marche entity.
     *
     */
    public function createAction(Request $request)
    {
        $entity = new Marche();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();

            /** @var \Laiso\armBundle\Entity\Marche $data */
            $data = $form->getData();

            $lotissement = $data->getLotissement();

            if($lotissement->getValide() == true)
                throw new AccessDeniedException('Lotissement déjà validé');

            $marche = $this->getDoctrine()->getRepository('LaisoArmBundle:Marche')->findOneBy(array(
                'lotissement' => $lotissement->getId()
            ));

            // Avenant ZERO valide: Quantités initiales valides.
            if($marche->getAvenants()[0]->getValide()==true) {

                $marche->setEntreprise($data->getEntreprise());
                $marche->setNumero($data->getNumero());
                $marche->setExercice($data->getExercice());
                $marche->setFinancement($data->getFinancement());

                $marche->getLotissement()->setValide(true); // Marché
                $marche->getLotissement()->setPuValide(false); // Prix Unitaires

                $em->persist($marche->getLotissement());
                $em->persist($marche);
                $em->flush();

                return $this->redirect($this->generateUrl('marche_show', array('id' => $marche->getId())));
            }else{
                // TODO: Add a flash message
            }
        }

        return $this->render('LaisoArmBundle:Marche:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Creates a form to create a Marche entity.
     *
     * @param Marche $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(Marche $entity)
    {
        $form = $this->createForm(new MarcheType(), $entity, array(
            'action' => $this->generateUrl('marche_create'),
            'method' => 'POST',
        ));

        $form
            ->add('submit', 'submit', array('label' => 'Créer', 'attr' => array(
            'class' => 'button success place-right')))
        ;

        return $form;
    }

    /**
     * Displays a form to create a new Marche entity.
     *
     */
    public function newAction(Request $request)
    {
        $entity = new Marche();
        $form   = $this->createCreateForm($entity);

        if($request->isXmlHttpRequest())
            return $this->render('LaisoArmBundle:Marche/includes:new_marche.html.twig', array(
                'entity' => $entity,
                'form'   => $form->createView(),
            ));

        return $this->render('LaisoArmBundle:Marche:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Finds and displays a Marche entity.
     *
     */
    public function showAction(Marche $marche)
    {
        $deleteForm = $this->createDeleteForm($marche->getId());

        if($marche->getLotissement()->getValide() == false)
            throw new NotFoundHttpException('Marché non trouvé');

        return $this->render('LaisoArmBundle:Marche:show.html.twig', array(
            'entity'      => $marche,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     *
     *
     * @param Marche $marche
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function showDqeAction(Marche $marche){
        $dqe = new DQE();

        $tempConsistance = array();
        /** @var \Laiso\ArmBundle\Entity\Consistance $c */
        foreach($marche->getLotissement()->getConsistances() as $c){
            $tempConsistance['consistance'] = $c;
            $tempConsistance['lignes'] = array();

            $tempLigne = array();
            $tempLignes = array();
            /** @var \Laiso\ArmBundle\Entity\AffectationPrix $affectationPrix */
            foreach($c->getPrix() as $affectationPrix){
                $tempLigne['serie'] = $affectationPrix->getSerie();
                $tempLigne['prixUnitaire'] = $affectationPrix->getPrixUnitaire();
                $tempLigne['avenants'] = array();


                $tempAvenant = array();

                /** @var \Laiso\ArmBundle\Entity\LigneDQE $ligne */
                foreach($affectationPrix->getLignesDqe() as $ligne){
                    for($i = 0; $i < sizeof($marche->getAvenants()); $i++) {
                        $avenant = $this->getDoctrine()->getRepository('LaisoArmBundle:Avenant')->findOneBy(array(
                            'marche' => $marche->getId(),
                            'libelle' => $this->getDoctrine()->getRepository('LaisoArmBundle:LibelleAvenant')->findOneBy(array(
                                'numero' => $i,
                            )),
                        ));
                        if ($ligne->getAvenant()->getId() == $avenant->getId())
                            array_push($tempAvenant, $avenant);
                        /*else
                            array_push($tempAvenant, null);*/
                    }
                }
                $tempLigne['avenants'] = $tempAvenant;
                array_push($tempLignes, $tempLigne);
            }
            $tempConsistance['lignes'] = $tempLignes;
        }

        $dqe->setConsistances($tempConsistance);

        return $this->render('@LaisoArm/Marche/dqe.html.twig', array(
            'marche' => $marche,
            'dqe' => $dqe
        ));
    }

    /**
     *
     * @param Marche $marche
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function validatePuAction(Marche $marche)
    {
        $em = $this->getDoctrine()->getManager();

        /** @var \Laiso\ArmBundle\Entity\Avenant $avenant */
        $avenant = $em->getRepository('LaisoArmBundle:Avenant')->findOneBy(array(
            'marche' => $marche->getId(),
            'libelle' => $em->getRepository('LaisoArmBundle:LibelleAvenant')->findOneBy(array(
                'numero' => 0,
            )),
        ));

        $valide = true;

        foreach($avenant->getLignes() as $ligne){
            if($ligne->getPrix()->getPrixUnitaire() == null){
                $valide = false;

                // TODO : Add a flash message

                break;
            }
        }
        if($valide) {
            $lotissement = $marche->getLotissement()->setPuValide(true);
            $em->persist($lotissement);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('avenant_show', array(
            'numero' => 0,
            'marcheId' => $marche->getId(),
        )));
    }

    /**
     * Displays a form to edit an existing Marche entity.
     *
     */
    public function editAction(Request $request, Marche $marche)
    {
        $editForm = $this->createEditForm($marche);
        $deleteForm = $this->createDeleteForm($marche->getId());

        if($request->isXmlHttpRequest())
            return $this->render('LaisoArmBundle:Marche/includes:edit_marche.html.twig', array(
                'entity'      => $marche,
                'edit_form'   => $editForm->createView(),
                'delete_form' => $deleteForm->createView(),
            ));

        return $this->render('LaisoArmBundle:Marche:edit.html.twig', array(
            'entity'      => $marche,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
    * Creates a form to edit a Marche entity.
    *
    * @param Marche $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(Marche $entity)
    {
        $form = $this->createForm(new MarcheType(), $entity, array(
            'action' => $this->generateUrl('marche_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Metter à jour', 'attr' => array(
            'class' => 'button success place-right'
        )));

        return $form;
    }
    /**
     * Edits an existing Marche entity.
     *
     */
    public function updateAction(Request $request, Marche $marche)
    {
        $deleteForm = $this->createDeleteForm($marche->getId());
        $editForm = $this->createEditForm($marche);
        $editForm->handleRequest($request);

        $em = $this->getDoctrine()->getManager();

        if ($editForm->isValid()) {
            $em->flush();

            return $this->redirect($this->generateUrl('marche_show', array('id' => $marche->getId())));
        }

        return $this->render('LaisoArmBundle:Marche:edit.html.twig', array(
            'entity'      => $marche,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }
    /**
     * Deletes a Marche entity.
     *
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('LaisoArmBundle:Marche')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find Marche entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('marche'));
    }

    /**
     * Creates a form to delete a Marche entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('marche_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array('label' => 'Supprimer', 'attr' => array(
                'class' => 'button danger place-right   '
            )))
            ->getForm()
        ;
    }
}
