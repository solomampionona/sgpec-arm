<?php

namespace Laiso\ArmBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use Laiso\ArmBundle\Entity\ConstatDeMesure;
use Laiso\ArmBundle\Form\ConstatDeMesureType;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * ConstatDeMesure controller.
 *
 */
class ConstatDeMesureController extends Controller
{

    /**
     * Lists all ConstatDeMesure entities.
     *
     */
    public function indexAction($marcheId, $decompteId)
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('LaisoArmBundle:ConstatDeMesure')->findBy(array(
            'decompte' => $decompteId
        ));

        return $this->render('LaisoArmBundle:ConstatDeMesure:index.html.twig', array(
            'entities' => $entities,
            'decompte' => $em->getRepository('LaisoArmBundle:Decompte')->find($decompteId),
        ));
    }
    /**
     * Creates a new ConstatDeMesure entity.
     *
     */
    public function createAction(Request $request, $marcheId, $decompteId)
    {
        $entity = new ConstatDeMesure();
        $form = $this->createCreateForm($entity, $marcheId, $decompteId);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('constatdemesure_show', array(
                'id' => $entity->getId(),
                'marcheId' => $marcheId,
                'decompteId' => $decompteId
            )));
        }

        return $this->render('LaisoArmBundle:ConstatDeMesure:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Creates a form to create a ConstatDeMesure entity.
     *
     * @param ConstatDeMesure $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(ConstatDeMesure $entity, $marcheId, $decompteId)
    {
        $form = $this->createForm(new ConstatDeMesureType(), $entity, array(
            'action' => $this->generateUrl('constatdemesure_create', array(
                'marcheId' => $marcheId,
                'decompteId' => $decompteId
            )),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Créer', 'attr'=> array(
            'class' => 'button success place-right'
        )));

        return $form;
    }

    /**
     * Displays a form to create a new ConstatDeMesure entity.
     *
     */
    public function newAction($marcheId, $decompteId)
    {
        $entity = new ConstatDeMesure();
        $form   = $this->createCreateForm($entity, $marcheId, $decompteId);

        $decompte = $this->getDoctrine()->getRepository('LaisoArmBundle:Decompte')->find($decompteId);

        if(!$decompte)
            throw new NotFoundHttpException('Unable to find Decompte entity');

        return $this->render('LaisoArmBundle:ConstatDeMesure:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
            'decompte' => $decompte
        ));
    }

    /**
     * Finds and displays a ConstatDeMesure entity.
     *
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('LaisoArmBundle:ConstatDeMesure')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find ConstatDeMesure entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return $this->render('LaisoArmBundle:ConstatDeMesure:show.html.twig', array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing ConstatDeMesure entity.
     *
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('LaisoArmBundle:ConstatDeMesure')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find ConstatDeMesure entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return $this->render('LaisoArmBundle:ConstatDeMesure:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
    * Creates a form to edit a ConstatDeMesure entity.
    *
    * @param ConstatDeMesure $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(ConstatDeMesure $entity)
    {
        $form = $this->createForm(new ConstatDeMesureType(), $entity, array(
            'action' => $this->generateUrl('constatdemesure_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Update'));

        return $form;
    }
    /**
     * Edits an existing ConstatDeMesure entity.
     *
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('LaisoArmBundle:ConstatDeMesure')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find ConstatDeMesure entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();

            return $this->redirect($this->generateUrl('constatdemesure_edit', array('id' => $id)));
        }

        return $this->render('LaisoArmBundle:ConstatDeMesure:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }
    /**
     * Deletes a ConstatDeMesure entity.
     *
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('LaisoArmBundle:ConstatDeMesure')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find ConstatDeMesure entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('constatdemesure'));
    }

    /**
     * Creates a form to delete a ConstatDeMesure entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('constatdemesure_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array('label' => 'Delete'))
            ->getForm()
        ;
    }
}
