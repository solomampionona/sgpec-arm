<?php

namespace Laiso\ArmBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use Laiso\ArmBundle\Entity\Decompte;
use Laiso\ArmBundle\Form\DecompteType;

/**
 * Decompte controller.
 *
 */
class DecompteController extends Controller
{

    /**
     * Lists all Decompte entities.
     *
     */
    public function indexAction($marcheId)
    {
        $em = $this->getDoctrine()->getManager();

        /** @var \Laiso\ArmBundle\Entity\Marche $marche */
        $marche = $em->getRepository('LaisoArmBundle:Marche')->find($marcheId);

        $entities = array();
        /** @var \Laiso\ArmBundle\Entity\Avenant $avenant */
        foreach($marche->getAvenants() as $avenant){
            foreach($avenant->getDecomptes() as $decompte){
                array_push($entities, $decompte);
            }
        }

        return $this->render('LaisoArmBundle:Decompte:index.html.twig', array(
            'entities' => $entities,
            'marche'    => $marche,
            'marcheId' => $marcheId,
        ));
    }
    /**
     * Creates a new Decompte entity.
     *
     */
    public function createAction(Request $request, $marcheId)
    {
        $entity = new Decompte();
        $form = $this->createCreateForm($entity, $marcheId);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();

            /** @var \Laiso\ArmBundle\Entity\Marche $marche */
            $marche = $em->getRepository('LaisoArmBundle:Marche')->find($marcheId);

            $avenant = null;

            /** @var \Laiso\ArmBundle\Entity\Avenant $av */
            foreach ($marche->getAvenants() as $av) {
                $avenant = $av;
            }
            $entity->setAvenant($av);
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('decompte_show', array(
                'id' => $entity->getId(),
                'marcheId' => $marcheId
            )));
        }

        return $this->render('LaisoArmBundle:Decompte:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Creates a form to create a Decompte entity.
     *
     * @param Decompte $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(Decompte $entity, $marcheId)
    {
        $form = $this->createForm(new DecompteType(), $entity, array(
            'action' => $this->generateUrl('decompte_create', array(
                'marcheId' => $marcheId,
            )),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Créer', 'attr' => array(
            'class' => 'button success place-right'
        )));

        return $form;
    }

    /**
     * Displays a form to create a new Decompte entity.
     *
     */
    public function newAction(Request $request, $marcheId)
    {
        $entity = new Decompte();
        $form   = $this->createCreateForm($entity, $marcheId);

        if($request->isXmlHttpRequest())
            return $this->render('LaisoArmBundle:Decompte/includes:new_decompte.html.twig', array(
                'entity' => $entity,
                'form'   => $form->createView(),
                'marcheId' => $marcheId
            ));

        return $this->render('LaisoArmBundle:Decompte:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
            'marcheId' => $marcheId
        ));
    }

    /**
     * Finds and displays a Decompte entity.
     *
     */
    public function showAction($id, $marcheId)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('LaisoArmBundle:Decompte')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Decompte entity.');
        }
        $deleteForm = $this->createDeleteForm($id, $marcheId);

        return $this->render('LaisoArmBundle:Decompte:show.html.twig', array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing Decompte entity.
     *
     */
    public function editAction(Request $request, $id, $marcheId)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('LaisoArmBundle:Decompte')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Decompte entity.');
        }

        $editForm = $this->createEditForm($entity, $marcheId);
        $deleteForm = $this->createDeleteForm($id, $marcheId);

        if($request->isXmlHttpRequest())
            return $this->render('LaisoArmBundle:Decompte/includes:edit_decompte.html.twig', array(
                'entity'      => $entity,
                'edit_form'   => $editForm->createView(),
                'delete_form' => $deleteForm->createView(),
            ));

        return $this->render('LaisoArmBundle:Decompte:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
    * Creates a form to edit a Decompte entity.
    *
    * @param Decompte $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(Decompte $entity, $marcheId)
    {
        $form = $this->createForm(new DecompteType(), $entity, array(
            'action' => $this->generateUrl('decompte_update', array(
                'id' => $entity->getId(),
                'marcheId' => $marcheId,
            )),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Mettre à jour', 'attr' => array(
            'class' => 'button success place-right'
        )));

        return $form;
    }
    /**
     * Edits an existing Decompte entity.
     *
     */
    public function updateAction(Request $request, $id, $marcheId)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('LaisoArmBundle:Decompte')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Decompte entity.');
        }

        $deleteForm = $this->createDeleteForm($id, $marcheId);
        $editForm = $this->createEditForm($entity, $marcheId);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();

            return $this->redirect($this->generateUrl('decompte_show', array(
                'id' => $id,
                'marcheId' => $marcheId
            )));
        }

        return $this->render('LaisoArmBundle:Decompte:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }
    /**
     * Deletes a Decompte entity.
     *
     */
    public function deleteAction(Request $request, $id, $marcheId)
    {
        $form = $this->createDeleteForm($id, $marcheId);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('LaisoArmBundle:Decompte')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find Decompte entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('decompte', array('marcheId' => $marcheId)));
    }

    /**
     * Creates a form to delete a Decompte entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id, $marcheId)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('decompte_delete', array(
                'id' => $id,
                'marcheId' => $marcheId
            )))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array('label' => 'Supprimer', 'attr' => array(
                'class' => 'danger place-right button'
            )))
            ->getForm()
        ;
    }
}
