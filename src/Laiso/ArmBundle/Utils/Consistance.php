<?php
/**
 * Created by PhpStorm.
 * User: laiso
 * Date: 26/12/2015
 * Time: 18:15
 */

namespace Laiso\ArmBundle\Utils;


class Consistance
{
    private $localisations;

    private $lignes;

    public function __construct()
    {
        $this->lignes = array();
        $this->localisations = array();
    }

    /**
     * @return array
     */
    public function getLocalisations()
    {
        return $this->localisations;
    }

    /**
     * @param array $localisations
     * @return Consistance
     */
    public function setLocalisations($localisations)
    {
        $this->localisations = $localisations;
        return $this;
    }

    /**
     * @return array
     */
    public function getLignes()
    {
        return $this->lignes;
    }

    /**
     * @param array $lignes
     * @return Consistance
     */
    public function setLignes($lignes)
    {
        $this->lignes = $lignes;
        return $this;
    }
}