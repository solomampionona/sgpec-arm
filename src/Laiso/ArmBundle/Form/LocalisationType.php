<?php

namespace Laiso\ArmBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class LocalisationType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('pKDebut', null, array(
                'attr' => array(
                    'data-validate-func' => "number",
                    'data-validate-hint'=>"PK début non valide"
                )
            ))
            ->add('pKFin', null, array(
                'attr' => array(
                    'data-validate-func' => "number",
                    'data-validate-hint'=>"PK fin non valide"
                )
            ))
            ->add('localisation', null, array(
                'attr' => array(
                    'data-validate-func' => "minlength",
                    'data-validate-arg' => 5,
                    'data-validate-hint'=>"La localisation doit avoir au moins 5 caractères de lonqueur²"
                )
            ))
            ->add('consistance')
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Laiso\ArmBundle\Entity\Localisation'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'laiso_armbundle_localisation';
    }
}
