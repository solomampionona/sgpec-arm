<?php

namespace Laiso\ArmBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class AxeType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('type', 'choice', array(
                'choices' => array(
                    'RNP' => 'RNP',
                    'RNS' => 'RNS',
                )
            ))
            ->add('numero')
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Laiso\ArmBundle\Entity\Axe'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'laiso_armbundle_axe';
    }
}
