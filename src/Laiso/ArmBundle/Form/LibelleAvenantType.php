<?php

namespace Laiso\ArmBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class LibelleAvenantType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('numero', null, array(
                'attr' => array(
                    'data-validate-func' => "number",
                    'data-validate-hint'=>"Numéro de libellé non valide"
                )
            ))
            ->add('libelle', null, array(
                'attr' => array(
                    'data-validate-func' => "minlength",
                    'data-validate-arg' => 14,
                    'data-validate-hint'=>"Libellé de marché non valide"
                )
            ))
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Laiso\ArmBundle\Entity\LibelleAvenant'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'laiso_armbundle_libelleavenant';
    }
}
