<?php

namespace Laiso\ArmBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * Decompte
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Laiso\ArmBundle\Repository\DecompteRepository")
 */
class Decompte
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="Numero", type="integer")
     */
    private $numero;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="DateDecompte", type="date")
     */
    private $dateDecompte;

    function __toString()
    {
        return $this->id ."";
    }



    /***********************************************
     *                   ASSOCIATIONS
     *
     *    Ne surtout pas modifier les annotations
     *      sauf en cas de modification du modèle
     *
     *                  (c) Laiso
     ***********************************************/


    /**
     * Decompte - Marché (Avenant ZERO)
     *
     * @ORM\ManyToOne(targetEntity="Laiso\ArmBundle\Entity\Avenant", inversedBy="decomptes")
     * @ORM\JoinColumn(nullable=false, referencedColumnName="id")
     */
    private $avenant;

    /**
     * @var
     * @ORM\OneToMany(targetEntity="Laiso\ArmBundle\Entity\ConstatDeMesure", mappedBy="decompte")
     */
    private $constatsDeMesure;

    public function __construct()
    {
        $this->constatsDeMesure = new ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set numero
     *
     * @param integer $numero
     *
     * @return Decompte
     */
    public function setNumero($numero)
    {
        $this->numero = $numero;

        return $this;
    }

    /**
     * Get numero
     *
     * @return integer
     */
    public function getNumero()
    {
        return $this->numero;
    }

    /**
     * Set dateDecompte
     *
     * @param \DateTime $dateDecompte
     *
     * @return Decompte
     */
    public function setDateDecompte($dateDecompte)
    {
        $this->dateDecompte = $dateDecompte;

        return $this;
    }

    /**
     * Get dateDecompte
     *
     * @return \DateTime
     */
    public function getDateDecompte()
    {
        return $this->dateDecompte;
    }

    /**
     * Set avenant
     *
     * @param \Laiso\ArmBundle\Entity\Avenant $avenant
     *
     * @return Decompte
     */
    public function setAvenant(\Laiso\ArmBundle\Entity\Avenant $avenant)
    {
        $this->avenant = $avenant;

        return $this;
    }

    /**
     * Get avenant
     *
     * @return \Laiso\ArmBundle\Entity\Avenant
     */
    public function getAvenant()
    {
        return $this->avenant;
    }

    /**
     * Add constatsDeMesure
     *
     * @param \Laiso\ArmBundle\Entity\ConstatDeMesure $constatsDeMesure
     *
     * @return Decompte
     */
    public function addConstatsDeMesure(\Laiso\ArmBundle\Entity\ConstatDeMesure $constatsDeMesure)
    {
        $this->constatsDeMesure[] = $constatsDeMesure;

        return $this;
    }

    /**
     * Remove constatsDeMesure
     *
     * @param \Laiso\ArmBundle\Entity\ConstatDeMesure $constatsDeMesure
     */
    public function removeConstatsDeMesure(\Laiso\ArmBundle\Entity\ConstatDeMesure $constatsDeMesure)
    {
        $this->constatsDeMesure->removeElement($constatsDeMesure);
    }

    /**
     * Get constatsDeMesure
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getConstatsDeMesure()
    {
        return $this->constatsDeMesure;
    }
}
