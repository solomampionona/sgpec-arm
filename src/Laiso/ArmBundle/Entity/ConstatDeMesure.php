<?php

namespace Laiso\ArmBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * ConstatDeMesure
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Laiso\ArmBundle\Repository\ConstatDeMesureRepository")
 */
class ConstatDeMesure
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="Numero", type="integer")
     */
    private $numero;

    /***********************************************
     *                   ASSOCIATIONS
     *
     *    Ne surtout pas modifier les annotations
     *      sauf en cas de modification du modèle
     *
     *                  (c) Laiso
     ***********************************************/


    /**
     * Decompte = ensemble de Constat de mesure
     *
     * @ORM\ManyToOne(targetEntity="Laiso\ArmBundle\Entity\Decompte", inversedBy="constatsDeMesure")
     * @ORM\JoinColumn(nullable=false, referencedColumnName="id")
     */
    private $decompte;

    /**
     * @var
     * @ORM\OneToMany(targetEntity="Laiso\ArmBundle\Entity\Attachement", mappedBy="constatDeMesure")
     */
    private $attachements;

    public function __construct()
    {
        $this->attachements = new ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set numero
     *
     * @param integer $numero
     *
     * @return ConstatDeMesure
     */
    public function setNumero($numero)
    {
        $this->numero = $numero;

        return $this;
    }

    /**
     * Get numero
     *
     * @return integer
     */
    public function getNumero()
    {
        return $this->numero;
    }

    /**
     * Set decompte
     *
     * @param \Laiso\ArmBundle\Entity\Decompte $decompte
     *
     * @return ConstatDeMesure
     */
    public function setDecompte(\Laiso\ArmBundle\Entity\Decompte $decompte)
    {
        $this->decompte = $decompte;

        return $this;
    }

    /**
     * Get decompte
     *
     * @return \Laiso\ArmBundle\Entity\Decompte
     */
    public function getDecompte()
    {
        return $this->decompte;
    }

    /**
     * Add attachement
     *
     * @param \Laiso\ArmBundle\Entity\Attachement $attachement
     *
     * @return ConstatDeMesure
     */
    public function addAttachement(\Laiso\ArmBundle\Entity\Attachement $attachement)
    {
        $this->attachements[] = $attachement;

        return $this;
    }

    /**
     * Remove attachement
     *
     * @param \Laiso\ArmBundle\Entity\Attachement $attachement
     */
    public function removeAttachement(\Laiso\ArmBundle\Entity\Attachement $attachement)
    {
        $this->attachements->removeElement($attachement);
    }

    /**
     * Get attachements
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getAttachements()
    {
        return $this->attachements;
    }
}
