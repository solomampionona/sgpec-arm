<?php

namespace Laiso\ArmBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * Attachement
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Laiso\ArmBundle\Repository\AttachementRepository")
 */
class Attachement
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="Numero", type="integer")
     */
    private $numero;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="DateAttachement", type="date")
     */
    private $dateAttachement;


    /***********************************************
     *                   ASSOCIATIONS
     *
     *    Ne surtout pas modifier les annotations
     *      sauf en cas de modification du modèle
     *
     *                  (c) Laiso
     ***********************************************/


    /**
     * @var
     * @ORM\ManyToOne(targetEntity="Laiso\ArmBundle\Entity\ConstatDeMesure", inversedBy="attachements")
     * @ORM\JoinColumn(nullable=false, referencedColumnName="id")
     */
    private $constatDeMesure;

    /**
     * @var
     *
     * @ORM\OneToMany(targetEntity="Laiso\ArmBundle\Entity\LigneAttachement", mappedBy="attachement")
     */
    private $lignes;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set numero
     *
     * @param integer $numero
     *
     * @return Attachement
     */
    public function setNumero($numero)
    {
        $this->numero = $numero;

        return $this;
    }

    /**
     * Get numero
     *
     * @return integer
     */
    public function getNumero()
    {
        return $this->numero;
    }

    /**
     * Set dateAttachement
     *
     * @param \DateTime $dateAttachement
     *
     * @return Attachement
     */
    public function setDateAttachement($dateAttachement)
    {
        $this->dateAttachement = $dateAttachement;

        return $this;
    }

    /**
     * Get dateAttachement
     *
     * @return \DateTime
     */
    public function getDateAttachement()
    {
        return $this->dateAttachement;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->constatDeMesure = new \Doctrine\Common\Collections\ArrayCollection();
        $this->lignes = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add constatDeMesure
     *
     * @param \Laiso\ArmBundle\Entity\ConstatDeMesure $constatDeMesure
     *
     * @return Attachement
     */
    public function addConstatDeMesure(\Laiso\ArmBundle\Entity\ConstatDeMesure $constatDeMesure)
    {
        $this->constatDeMesure[] = $constatDeMesure;

        return $this;
    }

    /**
     * Remove constatDeMesure
     *
     * @param \Laiso\ArmBundle\Entity\ConstatDeMesure $constatDeMesure
     */
    public function removeConstatDeMesure(\Laiso\ArmBundle\Entity\ConstatDeMesure $constatDeMesure)
    {
        $this->constatDeMesure->removeElement($constatDeMesure);
    }

    /**
     * Get constatDeMesure
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getConstatDeMesure()
    {
        return $this->constatDeMesure;
    }

    /**
     * Add ligne
     *
     * @param \Laiso\ArmBundle\Entity\LigneAttachement $ligne
     *
     * @return Attachement
     */
    public function addLigne(\Laiso\ArmBundle\Entity\LigneAttachement $ligne)
    {
        $this->lignes[] = $ligne;

        return $this;
    }

    /**
     * Remove ligne
     *
     * @param \Laiso\ArmBundle\Entity\LigneAttachement $ligne
     */
    public function removeLigne(\Laiso\ArmBundle\Entity\LigneAttachement $ligne)
    {
        $this->lignes->removeElement($ligne);
    }

    /**
     * Get lignes
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getLignes()
    {
        return $this->lignes;
    }

    /**
     * Set constatDeMesure
     *
     * @param \Laiso\ArmBundle\Entity\ConstatDeMesure $constatDeMesure
     *
     * @return Attachement
     */
    public function setConstatDeMesure(\Laiso\ArmBundle\Entity\ConstatDeMesure $constatDeMesure)
    {
        $this->constatDeMesure = $constatDeMesure;

        return $this;
    }
}
