<?php

namespace Laiso\ArmBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * LigneDQE
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Laiso\ArmBundle\Repository\LigneDQERepository")
 */
class LigneDQE
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="Quantite", type="float")
     */
    private $quantite;


    /***********************************************
     *                   ASSOCIATIONS
     *
     *    Ne surtout pas modifier les annotations
     *      sauf en cas de modification du modèle
     *
     *                  (c) Laiso
     ***********************************************/

    /**
     * LigneDQE - Avenant
     *
     * @ORM\ManyToOne(targetEntity="Laiso\ArmBundle\Entity\Avenant", inversedBy="lignes")
     * @ORM\JoinColumn(nullable=true)
     */
    private $avenant;

    /**
     * LigneDQE - Affectation Prix
     *
     * @ORM\ManyToOne(targetEntity="Laiso\ArmBundle\Entity\AffectationPrix", inversedBy="lignesDqe")
     * @ORM\JoinColumn(nullable=false)
     */
    private $prix;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set quantite
     *
     * @param string $quantite
     *
     * @return LigneDQE
     */
    public function setQuantite($quantite)
    {
        $this->quantite = $quantite;

        return $this;
    }

    /**
     * Get quantite
     *
     * @return string
     */
    public function getQuantite()
    {
        return $this->quantite;
    }

    /**
     * Set avenant
     *
     * @param \Laiso\ArmBundle\Entity\Avenant $avenant
     *
     * @return LigneDQE
     */
    public function setAvenant(\Laiso\ArmBundle\Entity\Avenant $avenant)
    {
        $this->avenant = $avenant;

        return $this;
    }

    /**
     * Get avenant
     *
     * @return \Laiso\ArmBundle\Entity\Avenant
     */
    public function getAvenant()
    {
        return $this->avenant;
    }

    /**
     * Set prix
     *
     * @param \Laiso\ArmBundle\Entity\AffectationPrix $prix
     *
     * @return LigneDQE
     */
    public function setPrix(\Laiso\ArmBundle\Entity\AffectationPrix $prix)
    {
        $this->prix = $prix;

        return $this;
    }

    /**
     * Get prix
     *
     * @return \Laiso\ArmBundle\Entity\AffectationPrix
     */
    public function getPrix()
    {
        return $this->prix;
    }

    function __toString()
    {
        $prix = $this->prix->getSerie();
        return $prix->getNumero()." - ".$prix->getDesignation();
    }


}
