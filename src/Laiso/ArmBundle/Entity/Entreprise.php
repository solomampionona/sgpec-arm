<?php

namespace Laiso\ArmBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * Entreprise
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Laiso\ArmBundle\Repository\EntrepriseRepository")
 */
class Entreprise
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="Denomination", type="string", length=100)
     */
    private $denomination;

    /**
     * @var string
     *
     * @ORM\Column(name="Sigle", type="string", length=50)
     */
    private $sigle;

    /**
     * @var string
     *
     * @ORM\Column(name="Adresse", type="string", length=100)
     */
    private $adresse;

    /**
     * @var string
     *
     * @ORM\Column(name="Statistique", type="string", length=20)
     */
    private $statistique;

    /**
     * @var string
     *
     * @ORM\Column(name="NIF", type="string", length=20)
     */
    private $nIF;

    /**
     * @var string
     *
     * @ORM\Column(name="Telephone1", type="string", length=15)
     */
    private $telephone1;

    /**
     * @var string
     *
     * @ORM\Column(name="Telephone2", type="string", length=15)
     */
    private $telephone2;

    /**
     * @var string
     *
     * @ORM\Column(name="Email", type="string", length=50)
     */
    private $email;


    /***********************************************
     *                   ASSOCIATIONS
     *
     *    Ne surtout pas modifier les annotations
     *      sauf en cas de modification du modèle
     *
     *                  (c) Laiso
     ***********************************************/

    /**
     *
     * @ORM\OneToMany(targetEntity="Laiso\ArmBundle\Entity\Marche", mappedBy="entreprise")
     */
    private $marches;

    public function __construct()
    {
        $this->marches = new ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set denomination
     *
     * @param string $denomination
     *
     * @return Entreprise
     */
    public function setDenomination($denomination)
    {
        $this->denomination = $denomination;

        return $this;
    }

    /**
     * Get denomination
     *
     * @return string
     */
    public function getDenomination()
    {
        return $this->denomination;
    }

    /**
     * Set sigle
     *
     * @param string $sigle
     *
     * @return Entreprise
     */
    public function setSigle($sigle)
    {
        $this->sigle = $sigle;

        return $this;
    }

    /**
     * Get sigle
     *
     * @return string
     */
    public function getSigle()
    {
        return $this->sigle;
    }

    /**
     * Set adresse
     *
     * @param string $adresse
     *
     * @return Entreprise
     */
    public function setAdresse($adresse)
    {
        $this->adresse = $adresse;

        return $this;
    }

    /**
     * Get adresse
     *
     * @return string
     */
    public function getAdresse()
    {
        return $this->adresse;
    }

    /**
     * Set statistique
     *
     * @param string $statistique
     *
     * @return Entreprise
     */
    public function setStatistique($statistique)
    {
        $this->statistique = $statistique;

        return $this;
    }

    /**
     * Get statistique
     *
     * @return string
     */
    public function getStatistique()
    {
        return $this->statistique;
    }

    /**
     * Set nIF
     *
     * @param string $nIF
     *
     * @return Entreprise
     */
    public function setNIF($nIF)
    {
        $this->nIF = $nIF;

        return $this;
    }

    /**
     * Get nIF
     *
     * @return string
     */
    public function getNIF()
    {
        return $this->nIF;
    }

    /**
     * Set telephone1
     *
     * @param string $telephone1
     *
     * @return Entreprise
     */
    public function setTelephone1($telephone1)
    {
        $this->telephone1 = $telephone1;

        return $this;
    }

    /**
     * Get telephone1
     *
     * @return string
     */
    public function getTelephone1()
    {
        return $this->telephone1;
    }

    /**
     * Set telephone2
     *
     * @param string $telephone2
     *
     * @return Entreprise
     */
    public function setTelephone2($telephone2)
    {
        $this->telephone2 = $telephone2;

        return $this;
    }

    /**
     * Get telephone2
     *
     * @return string
     */
    public function getTelephone2()
    {
        return $this->telephone2;
    }

    /**
     * Set email
     *
     * @param string $email
     *
     * @return Entreprise
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get email
     *
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Add marche
     *
     * @param \Laiso\ArmBundle\Entity\Marche $march
     *
     * @return Entreprise
     */
    public function addMarche(\Laiso\ArmBundle\Entity\Marche $march)
    {
        $this->marches[] = $march;

        return $this;
    }

    /**
     * Remove marche
     *
     * @param \Laiso\ArmBundle\Entity\Marche $march
     */
    public function removeMarche(\Laiso\ArmBundle\Entity\Marche $march)
    {
        $this->marches->removeElement($march);
    }

    /**
     * Get marches
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getMarches()
    {
        return $this->marches;
    }

    function __toString()
    {
        return $this->denomination . " (" .$this->sigle. ")";
    }

    /**
     * Add march
     *
     * @param \Laiso\ArmBundle\Entity\Marche $march
     *
     * @return Entreprise
     */
    public function addMarch(\Laiso\ArmBundle\Entity\Marche $march)
    {
        $this->marches[] = $march;

        return $this;
    }

    /**
     * Remove march
     *
     * @param \Laiso\ArmBundle\Entity\Marche $march
     */
    public function removeMarch(\Laiso\ArmBundle\Entity\Marche $march)
    {
        $this->marches->removeElement($march);
    }
}
