<?php

namespace Laiso\ArmBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * Marche
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Laiso\ArmBundle\Repository\MarcheRepository")
 */
class Marche
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="Numero", type="integer", nullable=true)
     */
    private $numero;


    /***********************************************
     *                   ASSOCIATIONS
     *
     *    Ne surtout pas modifier les annotations
     *      sauf en cas de modification du modèle
     *
     *                  (c) Laiso
     ***********************************************/


    /**
     * Un marché est exercé par une et une seule entreprise,
     * mais une entreprise peut exercer plusieurs marchés
     *
     * Nullable à true pour la création de l'offre
     *
     * @ORM\ManyToOne(targetEntity="Laiso\ArmBundle\Entity\Entreprise", inversedBy="marches")
     * @ORM\JoinColumn(nullable=true, referencedColumnName="id")
     */
    private $entreprise;

    /**
     * @var
     *
     * @ORM\ManyToOne(targetEntity="Laiso\ArmBundle\Entity\Campagne", inversedBy="marches")
     * @ORM\JoinColumn(nullable=false, referencedColumnName="id")
     */
    private $campagne;


    /**
     * Un marché est associeé à un seul lotissement (DAO)
     *
     * @ORM\OneToOne(targetEntity="Laiso\ArmBundle\Entity\Lotissement")
     */
    private $lotissement;

    /**
     * @var
     * @ORM\OneToMany(targetEntity="Laiso\ArmBundle\Entity\Avenant", mappedBy="marche")
     */
    private $avenants;

    /**
     * Marche constructor.
     */
    public function __construct()
    {
        $this->avenants = new ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set numero
     *
     * @param integer $numero
     *
     * @return Marche
     */
    public function setNumero($numero)
    {
        $this->numero = $numero;

        return $this;
    }

    /**
     * Get numero
     *
     * @return integer
     */
    public function getNumero()
    {
        return $this->numero;
    }

    /**
     * Set entreprise
     *
     * @param \Laiso\ArmBundle\Entity\Entreprise $entreprise
     *
     * @return Marche
     */
    public function setEntreprise(\Laiso\ArmBundle\Entity\Entreprise $entreprise = null)
    {
        $this->entreprise = $entreprise;

        return $this;
    }

    /**
     * Get entreprise
     *
     * @return \Laiso\ArmBundle\Entity\Entreprise
     */
    public function getEntreprise()
    {
        return $this->entreprise;
    }


    function __toString()
    {
        return "Marché n°" . $this->numero;
    }

    /**
     * Set lotissement
     *
     * @param \Laiso\ArmBundle\Entity\Lotissement $lotissement
     *
     * @return Marche
     */
    public function setLotissement(\Laiso\ArmBundle\Entity\Lotissement $lotissement = null)
    {
        $this->lotissement = $lotissement;

        return $this;
    }

    /**
     * Get lotissement
     *
     * @return \Laiso\ArmBundle\Entity\Lotissement
     */
    public function getLotissement()
    {
        return $this->lotissement;
    }

    /**
     * Add avenant
     *
     * @param \Laiso\ArmBundle\Entity\Avenant $avenant
     *
     * @return Marche
     */
    public function addAvenant(\Laiso\ArmBundle\Entity\Avenant $avenant)
    {
        $this->avenants[] = $avenant;

        return $this;
    }

    /**
     * Remove avenant
     *
     * @param \Laiso\ArmBundle\Entity\Avenant $avenant
     */
    public function removeAvenant(\Laiso\ArmBundle\Entity\Avenant $avenant)
    {
        $this->avenants->removeElement($avenant);
    }

    /**
     * Get avenants
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getAvenants()
    {
        return $this->avenants;
    }

    /**
     * Set campagne
     *
     * @param \Laiso\ArmBundle\Entity\Campagne $campagne
     *
     * @return Marche
     */
    public function setCampagne(\Laiso\ArmBundle\Entity\Campagne $campagne)
    {
        $this->campagne = $campagne;

        return $this;
    }

    /**
     * Get campagne
     *
     * @return \Laiso\ArmBundle\Entity\Campagne
     */
    public function getCampagne()
    {
        return $this->campagne;
    }
}
