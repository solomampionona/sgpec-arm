<?php

namespace Laiso\ArmBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Axe
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Laiso\ArmBundle\Repository\AxeRepository")
 */
class Axe
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="Type", type="string", length=5)
     */
    private $type;

    /**
     * @var integer
     *
     * @ORM\Column(name="Numero", type="integer")
     */
    private $numero;

    /***********************************************
     *                   ASSOCIATIONS
     *
     *    Ne surtout pas modifier les annotations
     *      sauf en cas de modification du modèle
     *
     *                  (c) Laiso
     ***********************************************/

    /**
     * @ORM\OneToMany(targetEntity="Laiso\ArmBundle\Entity\Lotissement", mappedBy="axe")
     */
    private $lotissements;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set type
     *
     * @param string $type
     *
     * @return Axe
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set numero
     *
     * @param integer $numero
     *
     * @return Axe
     */
    public function setNumero($numero)
    {
        $this->numero = $numero;

        return $this;
    }

    /**
     * Get numero
     *
     * @return integer
     */
    public function getNumero()
    {
        return $this->numero;
    }

    public function __toString()
    {
        return $this->type ." " . $this->numero;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->lotissements = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add lotissement
     *
     * @param \Laiso\ArmBundle\Entity\Lotissement $lotissement
     *
     * @return Axe
     */
    public function addLotissement(\Laiso\ArmBundle\Entity\Lotissement $lotissement)
    {
        $this->lotissements[] = $lotissement;

        return $this;
    }

    /**
     * Remove lotissement
     *
     * @param \Laiso\ArmBundle\Entity\Lotissement $lotissement
     */
    public function removeLotissement(\Laiso\ArmBundle\Entity\Lotissement $lotissement)
    {
        $this->lotissements->removeElement($lotissement);
    }

    /**
     * Get lotissements
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getLotissements()
    {
        return $this->lotissements;
    }
}
