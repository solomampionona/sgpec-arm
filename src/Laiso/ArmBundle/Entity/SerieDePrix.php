<?php

namespace Laiso\ArmBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * SerieDePrix
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Laiso\ArmBundle\Repository\SerieDePrixRepository")
 */
class SerieDePrix
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="Numero", type="integer")
     */
    private $numero;

    /**
     * @var string
     *
     * @ORM\Column(name="Designation", type="string", length=255)
     */
    private $designation;

    /**
     * @var string
     *
     * @ORM\Column(name="$sousCategorie", type="string", length=1, nullable=true)
     */
    private $sousCategorie;



    /***********************************************
     *                   ASSOCIATIONS
     *
     *    Ne surtout pas modifier les annotations
     *      sauf en cas de modification du modèle
     *
     *                  (c) Laiso
     ***********************************************/

    /**
     * @var
     * @ORM\ManyToOne(targetEntity="Laiso\ArmBundle\Entity\CategoriePrix", inversedBy="series")
     * @ORM\JoinColumn(nullable=false)
     */
    private $categorie;

    /**
     * @var
     * @ORM\ManyToOne(targetEntity="Laiso\ArmBundle\Entity\UniteDeMesure")
     * @ORM\JoinColumn(nullable=false)
     */
    private $unite;


    public function __toString()
    {
        return $this->numero . " - " . $this->designation;
    }


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set numero
     *
     * @param integer $numero
     *
     * @return SerieDePrix
     */
    public function setNumero($numero)
    {
        $this->numero = $numero;

        return $this;
    }

    /**
     * Get numero
     *
     * @return integer
     */
    public function getNumero()
    {
        return $this->numero;
    }

    /**
     * Set designation
     *
     * @param string $designation
     *
     * @return SerieDePrix
     */
    public function setDesignation($designation)
    {
        $this->designation = $designation;

        return $this;
    }

    /**
     * Get designation
     *
     * @return string
     */
    public function getDesignation()
    {
        return $this->designation;
    }

    /**
     * Set categorie
     *
     * @param \Laiso\ArmBundle\Entity\CategoriePrix $categorie
     *
     * @return SerieDePrix
     */
    public function setCategorie(\Laiso\ArmBundle\Entity\CategoriePrix $categorie)
    {
        $this->categorie = $categorie;

        return $this;
    }

    /**
     * Get categorie
     *
     * @return \Laiso\ArmBundle\Entity\CategoriePrix
     */
    public function getCategorie()
    {
        return $this->categorie;
    }

    /**
     * Set unite
     *
     * @param \Laiso\ArmBundle\Entity\UniteDeMesure $unite
     *
     * @return SerieDePrix
     */
    public function setUnite(\Laiso\ArmBundle\Entity\UniteDeMesure $unite)
    {
        $this->unite = $unite;

        return $this;
    }

    /**
     * Get unite
     *
     * @return \Laiso\ArmBundle\Entity\UniteDeMesure
     */
    public function getUnite()
    {
        return $this->unite;
    }

    /**
     * Set sousCategorie
     *
     * @param string $sousCategorie
     *
     * @return SerieDePrix
     */
    public function setSousCategorie($sousCategorie)
    {
        $this->sousCategorie = $sousCategorie;

        return $this;
    }

    /**
     * Get sousCategorie
     *
     * @return string
     */
    public function getSousCategorie()
    {
        return $this->sousCategorie;
    }
}
