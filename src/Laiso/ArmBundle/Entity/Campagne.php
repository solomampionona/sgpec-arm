<?php

namespace Laiso\ArmBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Campagne
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Laiso\ArmBundle\Repository\CampagneRepository")
 */
class Campagne
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="Exercice", type="string", length=20)
     */
    private $exercice;

    /**
     * @var string
     *
     * @ORM\Column(name="Financement", type="string", length=100)
     */
    private $financement;

    /**
     * @var float
     *
     * @ORM\Column(name="TVA", type="float")
     */
    private $tVA;

    /***********************************************
     *                   ASSOCIATIONS
     *
     *    Ne surtout pas modifier les annotations
     *      sauf en cas de modification du modèle
     *
     *                  (c) Laiso
     ***********************************************/

    /**
     * @var
     *
     * @ORM\OneToMany(targetEntity="Laiso\ArmBundle\Entity\Marche", mappedBy="campagne")
     */
    private $marches;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set exercice
     *
     * @param string $exercice
     *
     * @return Campagne
     */
    public function setExercice($exercice)
    {
        $this->exercice = $exercice;

        return $this;
    }

    /**
     * Get exercice
     *
     * @return string
     */
    public function getExercice()
    {
        return $this->exercice;
    }

    /**
     * Set financement
     *
     * @param string $financement
     *
     * @return Campagne
     */
    public function setFinancement($financement)
    {
        $this->financement = $financement;

        return $this;
    }

    /**
     * Get financement
     *
     * @return string
     */
    public function getFinancement()
    {
        return $this->financement;
    }

    /**
     * Set tVA
     *
     * @param float $tVA
     *
     * @return Campagne
     */
    public function setTVA($tVA)
    {
        $this->tVA = $tVA;

        return $this;
    }

    /**
     * Get tVA
     *
     * @return float
     */
    public function getTVA()
    {
        return $this->tVA;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->marches = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add march
     *
     * @param \Laiso\ArmBundle\Entity\Marche $march
     *
     * @return Campagne
     */
    public function addMarch(\Laiso\ArmBundle\Entity\Marche $march)
    {
        $this->marches[] = $march;

        return $this;
    }

    /**
     * Remove march
     *
     * @param \Laiso\ArmBundle\Entity\Marche $march
     */
    public function removeMarch(\Laiso\ArmBundle\Entity\Marche $march)
    {
        $this->marches->removeElement($march);
    }

    /**
     * Get marches
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getMarches()
    {
        return $this->marches;
    }
}
