<?php
// src/AppBundle/Entity/User.php

namespace Laiso\ArmBundle\Entity;

use FOS\UserBundle\Model\User as BaseUser;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="responsable")
 */
class Responsable extends BaseUser
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    public function __construct()
    {
        parent::__construct();
    }

    /**
     * @var string
     *
     * @ORM\Column(name="Nom", type="string", length=100, nullable=true)
     */
    private $nom;

    /**
     * @var string
     *
     * @ORM\Column(name="Prenom", type="string", length=100, nullable=true)
     */
    private $prenom;


    /***********************************************
     *                   ASSOCIATIONS
     *
     *    Ne surtout pas modifier les annotations
     *      sauf en cas de modification du modèle
     *
     *                  (c) Laiso
     ***********************************************/


    /**
     * Un marché est exercé par une et une seule entreprise,
     * mais une entreprise peut exercer plusieurs marchés
     *
     * Nullable à true pour la création de l'offre
     *
     * @ORM\ManyToOne(targetEntity="Laiso\ArmBundle\Entity\Bloc", inversedBy="responsables")
     * @ORM\JoinColumn(nullable=true)
     */
    private $bloc;

    /**
     * @ORM\ManyToMany(targetEntity="Laiso\ArmBundle\Entity\Groupe")
     * @ORM\JoinTable(name="responsable_groupe",
     *      joinColumns={@ORM\JoinColumn(name="responsable_id", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="groupe_id", referencedColumnName="id")}
     * )
     */
    protected $groups;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getNom()
    {
        return $this->nom;
    }

    /**
     * @param string $nom
     */
    public function setNom($nom)
    {
        $this->nom = $nom;
    }

    /**
     * @return string
     */
    public function getPrenom()
    {
        return $this->prenom;
    }

    /**
     * @param string $prenom
     */
    public function setPrenom($prenom)
    {
        $this->prenom = $prenom;
    }



    /**
     * Set bloc
     *
     * @param \Laiso\ArmBundle\Entity\Bloc $bloc
     *
     * @return Responsable
     */
    public function setBloc(\Laiso\ArmBundle\Entity\Bloc $bloc = null)
    {
        $this->bloc = $bloc;

        return $this;
    }

    /**
     * Get bloc
     *
     * @return \Laiso\ArmBundle\Entity\Bloc
     */
    public function getBloc()
    {
        return $this->bloc;
    }
}
