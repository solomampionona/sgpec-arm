<?php

namespace Laiso\ArmBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Region
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Laiso\ArmBundle\Repository\RegionRepository")
 */
class Region
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="Nom", type="string", length=100)
     */
    private $nom;

    /***********************************************
     *                   ASSOCIATIONS
     *
     *    Ne surtout pas modifier les annotations
     *      sauf en cas de modification du modèle
     *
     *                  (c) Laiso
     ***********************************************/


    /**
     * Une region appartient seulement à un et un seul bloc, mais un bloc peut
     * en contenir plusieurs
     *
     * @ORM\OnetoMany(targetEntity="Laiso\ArmBundle\Entity\Lotissement", mappedBy="region")
     */

    private $lotissements;

    /**
     * @var
     *
     * @ORM\ManyToOne(targetEntity="Laiso\ArmBundle\Entity\Bloc", inversedBy="regions")
     * @ORM\JoinColumn(nullable=false, referencedColumnName="id")
     */
    private $bloc;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nom
     *
     * @param string $nom
     *
     * @return Region
     */
    public function setNom($nom)
    {
        $this->nom = $nom;

        return $this;
    }

    /**
     * Get nom
     *
     * @return string
     */
    public function getNom()
    {
        return $this->nom;
    }
    
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->lotissements = new \Doctrine\Common\Collections\ArrayCollection();
    }

    function __toString()
    {
        return $this->nom;
    }


    /**
     * Add lotissement
     *
     * @param \Laiso\ArmBundle\Entity\Lotissement $lotissement
     *
     * @return Region
     */
    public function addLotissement(\Laiso\ArmBundle\Entity\Lotissement $lotissement)
    {
        $this->lotissements[] = $lotissement;

        return $this;
    }

    /**
     * Remove lotissement
     *
     * @param \Laiso\ArmBundle\Entity\Lotissement $lotissement
     */
    public function removeLotissement(\Laiso\ArmBundle\Entity\Lotissement $lotissement)
    {
        $this->lotissements->removeElement($lotissement);
    }

    /**
     * Get lotissements
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getLotissements()
    {
        return $this->lotissements;
    }

    /**
     * Set bloc
     *
     * @param \Laiso\ArmBundle\Entity\Bloc $bloc
     *
     * @return Region
     */
    public function setBloc(\Laiso\ArmBundle\Entity\Bloc $bloc = null)
    {
        $this->bloc = $bloc;

        return $this;
    }

    /**
     * Get bloc
     *
     * @return \Laiso\ArmBundle\Entity\Bloc
     */
    public function getBloc()
    {
        return $this->bloc;
    }
}
