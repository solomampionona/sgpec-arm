<?php

namespace Laiso\ArmBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * Classe
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Laiso\ArmBundle\Repository\ClasseRepository")
 */
class Classe
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="Classe", type="string", length=5)
     */
    private $classe;


    /***********************************************
     *                   ASSOCIATIONS
     *
     *    Ne surtout pas modifier les annotations
     *      sauf en cas de modification du modèle
     *
     *                  (c) Laiso
     ***********************************************/


    /**
     * @var
     * @ORM\OneToMany(targetEntity="Laiso\ArmBundle\Entity\Lotissement", mappedBy="classe")
     */
    private $lotissements;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set classe
     *
     * @param string $classe
     *
     * @return Classe
     */
    public function setClasse($classe)
    {
        $this->classe = $classe;

        return $this;
    }

    /**
     * Get classe
     *
     * @return string
     */
    public function getClasse()
    {
        return $this->classe;
    }

    /**
     *
     */
    function __toString()
    {
        return $this->classe;
    }


    /**
     * Constructor
     */
    public function __construct()
    {
        $this->lotissements = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add lotissement
     *
     * @param \Laiso\ArmBundle\Entity\Lotissement $lotissement
     *
     * @return Classe
     */
    public function addLotissement(\Laiso\ArmBundle\Entity\Lotissement $lotissement)
    {
        $this->lotissements[] = $lotissement;

        return $this;
    }

    /**
     * Remove lotissement
     *
     * @param \Laiso\ArmBundle\Entity\Lotissement $lotissement
     */
    public function removeLotissement(\Laiso\ArmBundle\Entity\Lotissement $lotissement)
    {
        $this->lotissements->removeElement($lotissement);
    }

    /**
     * Get lotissements
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getLotissements()
    {
        return $this->lotissements;
    }
}
