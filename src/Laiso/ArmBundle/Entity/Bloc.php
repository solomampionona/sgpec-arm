<?php

namespace Laiso\ArmBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * Bloc
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Laiso\ArmBundle\Repository\BlocRepository")
 */
class Bloc
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="Numero", type="integer", unique=true)
     */
    private $numero;

    /**
     * @var string
     *
     * @ORM\Column(name="Localisation", type="string", length=100)
     */
    private $localisation;

    /***********************************************
     *                   ASSOCIATIONS
     *
     *    Ne surtout pas modifier les annotations
     *      sauf en cas de modification du modèle
     *
     *                  (c) Laiso
     ***********************************************/

    /**
     * @var
     * @ORM\OneToMany(targetEntity="Laiso\ArmBundle\Entity\Lotissement", mappedBy="bloc")
     */
    private $lotissements;

    /**
     * @var
     * @ORM\OneToMany(targetEntity="Laiso\ArmBundle\Entity\Responsable", mappedBy="bloc")
     */
    private $responsables;

    /**
     * @var
     * @ORM\OneToMany(targetEntity="Laiso\ArmBundle\Entity\Region", mappedBy="bloc")
     */
    private $regions;

    /**
     * Bloc constructor.
     */

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set numero
     *
     * @param integer $numero
     *
     * @return Bloc
     */
    public function setNumero($numero)
    {
        $this->numero = $numero;

        return $this;
    }

    /**
     * Get numero
     *
     * @return integer
     */
    public function getNumero()
    {
        return $this->numero;
    }

    /**
     * Set localisation
     *
     * @param string $localisation
     *
     * @return Bloc
     */
    public function setLocalisation($localisation)
    {
        $this->localisation = $localisation;

        return $this;
    }

    /**
     * Get localisation
     *
     * @return string
     */
    public function getLocalisation()
    {
        return $this->localisation;
    }

    /**
     *
     * @return string
     */
    function __toString()
    {
        if($this->numero < 10)
            return "B-0" . $this->getNumero() . " - " . $this->localisation;
        return "B-" . $this->getNumero() . " - " . $this->localisation;
    }

    /**
     * Add lotissement
     *
     * @param \Laiso\ArmBundle\Entity\Lotissement $lotissement
     *
     * @return Bloc
     */
    public function addLotissement(\Laiso\ArmBundle\Entity\Lotissement $lotissement)
    {
        $this->lotissements[] = $lotissement;

        return $this;
    }

    /**
     * Remove lotissement
     *
     * @param \Laiso\ArmBundle\Entity\Lotissement $lotissement
     */
    public function removeLotissement(\Laiso\ArmBundle\Entity\Lotissement $lotissement)
    {
        $this->lotissements->removeElement($lotissement);
    }

    /**
     * Get lotissements
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getLotissements()
    {
        return $this->lotissements;
    }

    /**
     * Add region
     *
     * @param \Laiso\ArmBundle\Entity\Region $region
     *
     * @return Bloc
     */
    public function addRegion(\Laiso\ArmBundle\Entity\Region $region)
    {
        $this->regions[] = $region;

        return $this;
    }
    

    /**
     * Add responsable
     *
     * @param \Laiso\ArmBundle\Entity\Responsable $responsable
     *
     * @return Bloc
     */
    public function addResponsable(\Laiso\ArmBundle\Entity\Responsable $responsable)
    {
        $this->responsables[] = $responsable;

        return $this;
    }

    /**
     * Remove responsable
     *
     * @param \Laiso\ArmBundle\Entity\Responsable $responsable
     */
    public function removeResponsable(\Laiso\ArmBundle\Entity\Responsable $responsable)
    {
        $this->responsables->removeElement($responsable);
    }

    /**
     * Get responsables
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getResponsables()
    {
        return $this->responsables;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->lotissements = new \Doctrine\Common\Collections\ArrayCollection();
        $this->responsables = new \Doctrine\Common\Collections\ArrayCollection();
        $this->regions = new \Doctrine\Common\Collections\ArrayCollection();
    }


    /**
     * Remove region
     *
     * @param \Laiso\ArmBundle\Entity\Region $region
     */
    public function removeRegion(\Laiso\ArmBundle\Entity\Region $region)
    {
        $this->regions->removeElement($region);
    }

    /**
     * Get regions
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getRegions()
    {
        return $this->regions;
    }
}
