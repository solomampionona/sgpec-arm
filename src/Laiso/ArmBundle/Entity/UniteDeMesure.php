<?php

namespace Laiso\ArmBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * UniteDeMesure
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Laiso\ArmBundle\Repository\UniteDeMesureRepository")
 */
class UniteDeMesure
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="Unite", type="string", length=50)
     */
    private $unite;

    /**
     * @var string
     *
     * @ORM\Column(name="Abbrege", type="string", length=10)
     */
    private $abbrege;

    /**
     * @var string
     *
     * @ORM\Column(name="Arretage", type="string", length=100)
     */
    private $arretage;


    public function __toString()
    {
        return $this->unite;
    }


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set unite
     *
     * @param string $unite
     *
     * @return UniteDeMesure
     */
    public function setUnite($unite)
    {
        $this->unite = $unite;

        return $this;
    }

    /**
     * Get unite
     *
     * @return string
     */
    public function getUnite()
    {
        return $this->unite;
    }

    /**
     * Set arretage
     *
     * @param string $arretage
     *
     * @return UniteDeMesure
     */
    public function setArretage($arretage)
    {
        $this->arretage = $arretage;

        return $this;
    }

    /**
     * Get arretage
     *
     * @return string
     */
    public function getArretage()
    {
        return $this->arretage;
    }

    /**
     * Set abbrege
     *
     * @param string $abbrege
     *
     * @return UniteDeMesure
     */
    public function setAbbrege($abbrege)
    {
        $this->abbrege = $abbrege;

        return $this;
    }

    /**
     * Get abbrege
     *
     * @return string
     */
    public function getAbbrege()
    {
        return $this->abbrege;
    }
}
