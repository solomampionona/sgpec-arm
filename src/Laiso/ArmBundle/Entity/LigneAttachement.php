<?php

namespace Laiso\ArmBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * LigneAttachement
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Laiso\ArmBundle\Repository\LigneAttachementRepository")
 */
class LigneAttachement
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;


    /***********************************************
     *                   ASSOCIATIONS
     *
     *    Ne surtout pas modifier les annotations
     *      sauf en cas de modification du modèle
     *
     *                  (c) Laiso
     ***********************************************/


    /**
     *
     * 
     *
     * @ORM\ManyToOne(targetEntity="Laiso\ArmBundle\Entity\Attachement", inversedBy="lignes")
     * @ORM\JoinColumn(nullable=true, referencedColumnName="id")
     */
    private $attachement;

    /**
     *
     * @ORM\ManyToOne(targetEntity="Laiso\ArmBundle\Entity\LigneDQE")
     * @ORM\JoinColumn(nullable=true)
     */
    private $ligneDqe;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set attachement
     *
     * @param \Laiso\ArmBundle\Entity\Attachement $attachement
     *
     * @return LigneAttachement
     */
    public function setAttachement(\Laiso\ArmBundle\Entity\Attachement $attachement = null)
    {
        $this->attachement = $attachement;

        return $this;
    }

    /**
     * Get attachement
     *
     * @return \Laiso\ArmBundle\Entity\Attachement
     */
    public function getAttachement()
    {
        return $this->attachement;
    }

    /**
     * Set ligneDqe
     *
     * @param \Laiso\ArmBundle\Entity\LigneDQE $ligneDqe
     *
     * @return LigneAttachement
     */
    public function setLigneDqe(\Laiso\ArmBundle\Entity\LigneDQE $ligneDqe = null)
    {
        $this->ligneDqe = $ligneDqe;

        return $this;
    }

    /**
     * Get ligneDqe
     *
     * @return \Laiso\ArmBundle\Entity\LigneDQE
     */
    public function getLigneDqe()
    {
        return $this->ligneDqe;
    }
}
