<?php

namespace Laiso\ArmBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * Consistance
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Laiso\ArmBundle\Repository\ConsistanceRepository")
 */
class Consistance
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;



    /***********************************************
     *                   ASSOCIATIONS
     *
     *    Ne surtout pas modifier les annotations
     *      sauf en cas de modification du modèle
     *
     *                  (c) Laiso
     ***********************************************/


    /**
     * Un marché est exercé par une et une seule entreprise,
     * mais une entreprise peut exercer plusieurs marchés
     *
     * Nullable à true pour la création de l'offre
     *
     * @ORM\ManyToOne(targetEntity="Laiso\ArmBundle\Entity\Lotissement", inversedBy="consistances")
     * @ORM\JoinColumn(nullable=true, referencedColumnName="id")
     */
    private $lotissement;

    /**
     * @var string
     *
     * @ORM\ManyToMany(targetEntity="Laiso\ArmBundle\Entity\TypeConsistance")
     */
    private $types;

    /**
     * @var
     * @ORM\OneToMany(targetEntity="Laiso\ArmBundle\Entity\Localisation", mappedBy="consistance")
     */
    private $localisations;

    /**
     * @var
     * @ORM\OneToMany(targetEntity="Laiso\ArmBundle\Entity\AffectationPrix", mappedBy="consistance")
     */
    private $prix;

    public function __construct()
    {
        $this->localisations = new ArrayCollection();
        $this->prix = new ArrayCollection();
        $this->types = new ArrayCollection();
    }


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }


    /**
     * Set lotissement
     *
     * @param \Laiso\ArmBundle\Entity\Lotissement $lotissement
     *
     * @return Consistance
     */
    public function setLotissement(\Laiso\ArmBundle\Entity\Lotissement $lotissement = null)
    {
        $this->lotissement = $lotissement;

        return $this;
    }

    /**
     * Get lotissement
     *
     * @return \Laiso\ArmBundle\Entity\Lotissement
     */
    public function getLotissement()
    {
        return $this->lotissement;
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return "Consistance n°" . $this->id . " - " . $this->getLotissement()->__toString();
    }

    /**
     * Add localisation
     *
     * @param \Laiso\ArmBundle\Entity\Localisation $localisation
     *
     * @return Consistance
     */
    public function addLocalisation(\Laiso\ArmBundle\Entity\Localisation $localisation)
    {
        $this->localisations[] = $localisation;

        return $this;
    }

    /**
     * Remove localisation
     *
     * @param \Laiso\ArmBundle\Entity\Localisation $localisation
     */
    public function removeLocalisation(\Laiso\ArmBundle\Entity\Localisation $localisation)
    {
        $this->localisations->removeElement($localisation);
    }

    /**
     * Get localisations
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getLocalisations()
    {
        return $this->localisations;
    }

    /**
     * Add prix
     *
     * @param \Laiso\ArmBundle\Entity\AffectationPrix $prix
     *
     * @return Consistance
     */
    public function addPrix(\Laiso\ArmBundle\Entity\AffectationPrix $prix)
    {
        $this->prix[] = $prix;

        return $this;
    }

    /**
     * Remove prix
     *
     * @param \Laiso\ArmBundle\Entity\AffectationPrix $prix
     */
    public function removePrix(\Laiso\ArmBundle\Entity\AffectationPrix $prix)
    {
        $this->prix->removeElement($prix);
    }

    /**
     * Get prix
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getPrix()
    {
        return $this->prix;
    }

    /**
     * Add type
     *
     * @param \Laiso\ArmBundle\Entity\TypeConsistance $type
     *
     * @return Consistance
     */
    public function addType(\Laiso\ArmBundle\Entity\TypeConsistance $type)
    {
        $this->types[] = $type;

        return $this;
    }

    /**
     * Remove type
     *
     * @param \Laiso\ArmBundle\Entity\TypeConsistance $type
     */
    public function removeType(\Laiso\ArmBundle\Entity\TypeConsistance $type)
    {
        $this->types->removeElement($type);
    }

    /**
     * Get types
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getTypes()
    {
        return $this->types;
    }
}
